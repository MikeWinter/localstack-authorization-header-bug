variable "region" {
  type        = string
  description = "The AWS region to deploy into."
  default     = "eu-west-2"
}

variable "use_local_endpoints" {
  type        = bool
  description = "Determines whether to deploy locally or into AWS."
  default     = false
}

variable "lambda_version" {
  type        = string
  description = "The version of the Lambda function."

  validation {
    condition     = can(regex("^\\d+\\.\\d+\\.\\d+$", var.lambda_version))
    error_message = "The lambda_version value must be a semantic version number (i.e. \"<major>.<minor>.<patch>\")"
  }
}
