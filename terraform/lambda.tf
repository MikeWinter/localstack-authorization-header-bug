resource "aws_lambda_function" "auth" {
  function_name = "AuthExample"
  role          = aws_iam_role.auth_lambda.arn

  filename = "../dist/auth-lambda-${var.lambda_version}.zip"
  handler  = "auth_lambda.handler.get_authorization_header"
  runtime  = "python3.9"
}

resource "aws_lambda_permission" "toggles" {
  statement_id_prefix = "AllowAccessFromApiGateway-"

  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.auth.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_api_gateway_rest_api.example.execution_arn}/*/GET/*"
}

resource "aws_iam_role" "auth_lambda" {
  assume_role_policy = data.aws_iam_policy_document.lambda_assume.json
}

data "aws_iam_policy_document" "lambda_assume" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}
