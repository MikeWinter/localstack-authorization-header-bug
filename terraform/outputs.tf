output "gateway_url" {
  value = !var.use_local_endpoints ? aws_api_gateway_stage.example.invoke_url : "https://${aws_api_gateway_rest_api.example.id}.execute-api.localhost.localstack.cloud:4566/${aws_api_gateway_stage.example.stage_name}"
}
