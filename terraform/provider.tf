provider "aws" {
  region = var.region

  endpoints {
    apigateway = local.endpoint
    lambda     = local.endpoint
    iam        = local.endpoint
  }
}

locals {
  endpoint = var.use_local_endpoints ? "http://localhost.localstack.cloud:4566" : null
}
