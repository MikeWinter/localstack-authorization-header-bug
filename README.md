# LocalStack Authorization HTTP Header bug demo

This repository provides a minimal(-ish) demonstration of a suspected bug in
the processing of `Authorization` HTTP headers via REST (v1) API Gateways in
LocalStack.

The demonstration consists of a simple AWS Lambda function that returns the
value of the `Authorization` HTTP header received from the incoming request,
and Terraform configuration for setting up other related resources.

This example shows that a Lambda deployed to AWS will behave correctly, but the
behaviour differs in LocalStack. Instead of the expected header value,
LocalStack passes an empty string to the function. In addition, a workaround is
shown via the custom `X-Localstack-Authorization` header.

## Disclaimer

None of the code in this repository is suitable for production environments. It
is for demonstration purposes only. Use at your own risk.

## Requirements

* [LocalStack](https://docs.localstack.cloud/get-started/#installation)
* [Python 3.9](https://www.python.org/downloads/)
* [Poetry](https://python-poetry.org/docs/#installation) (or [via Homebrew](https://formulae.brew.sh/formula/poetry))
* [jq](https://stedolan.github.io/jq/download/)
* [Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli)

Poetry isn’t essential for building the Lambda used in this demonstration, but
the tooling provided assumes it’s installed. There are currently no Python
packages used besides the standard library, so building the ZIP required for
deployment is pretty trivial.

## Using this demonstration

Two scripts, [`cloud`](./cloud) and [`local`](./local), define a set of tasks
to simplify `build`ing, `deploy`ing, and `test`ing the Lambda function and API
Gateway in their namesake environments. The `local` script includes an extra
task, `show-workaround`, that repeats the `test` task using a custom HTTP
header (`X-Localstack-Authorization`) instead of the usual one
(`Authorization`).

The `local` script will start LocalStack if it’s not already running. However,
it will not stop it automatically. Both scripts provide a `destroy` task to
tear down their respective environments.

One may use the `help` subcommand with both scripts to list these options in
the terminal.

## Licencing

This code is released under the GNU General Public License v3.0 (or later). Please see
[GPL-3.0-or-later](LICENSES/GPL-3.0-or-later.txt) for more details.

Copyright © 2022 Mike Winter <mail@mikewinter.uk>

SPDX-License-Identifier: GPL-3.0-or-later
