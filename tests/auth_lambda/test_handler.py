import json

from auth_lambda.handler import get_authorization_header


def test_returns_authorization_header_value_when_present() -> None:
    request_event = {
        "headers": {
            "Authorization": "Bearer token",
        },
    }

    response = get_authorization_header(request_event, None)

    assert response.get("statusCode", 200) == 200
    assert json.loads(response["body"]) == {
        "auth": "Bearer token"
    }


def test_returns_authorization_header_regardless_of_case() -> None:
    request_event = {
        "headers": {
            "authorization": "Bearer another-token",
        },
    }

    response = get_authorization_header(request_event, None)

    assert json.loads(response["body"]) == {
        "auth": "Bearer another-token"
    }


def test_returns_not_authorized_if_authorization_header_is_absent() -> None:
    request_event = {
        "headers": {
            "Accept": "application/json",
        },
    }

    response = get_authorization_header(request_event, None)

    assert response["statusCode"] == 401
    assert response["headers"] == {
        "WWW-Authenticate": 'Bearer realm="test"',
    }
