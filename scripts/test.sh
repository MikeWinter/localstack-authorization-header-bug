#!/usr/bin/env bash

function test_lambda() {
  local -r url="$1"
  local -r auth_value="Bearer token"
  local response

  if ! response="$(_send_request "$url" "$auth_value")"; then
    _show_error_message "$response"
  fi

  _check_token "$response" "$auth_value"
}

function test_lambda_workaround() {
  local -r url="$1"
  local -r auth_value="Bearer token"
  local response

  if ! response="$(_send_request "$url" "$auth_value" "X-Localstack-")"; then
    _show_error_message "$response"
  fi

  _check_token "$response" "$auth_value"
}

function _send_request() {
  local -r url="$1"
  local -r token="$2"
  local -r header_prefix="${3:-}"

  curl --fail --silent --header "${header_prefix}Authorization: $token" "$url"
}

function _show_error_message() {
  local -r response="$1"
  local message

  message="$(echo "$response" | jq -r .error)"
  echo "Retrieving a response from the Lambda function failed: $message" 1>&2
  exit 32
}

function _check_token() {
  local -r response="$1"
  local -r expected_token="$2"
  local token

  token="$(echo "$response" | jq -r .auth)"
  if [[ "$token" == "$expected_token" ]]; then
    echo "The Lambda returned the expected result!" 1>&2
  else
    echo "The Lambda returned an unexpected result: '$token'" 1>&2
    exit 1
  fi
}
