#!/usr/bin/env bash
set -eu

function with_temporary_directory() {
  local -r command="$1"
  local -a args
  local tmp_path
  shift
  args=( "$@" )

  # shellcheck disable=SC2154
  tmp_path="$(mktemp -d -t "$script_name")"
  trap 'rm -rf $tmp_path' EXIT ERR
  $command "$tmp_path" "${args[@]}"
  trap - EXIT ERR
  rm -rf "$tmp_path"
}

function get_lambda_version() {
  # shellcheck disable=SC2154
  (cd "$abs_script_path"; poetry version --short)
}
