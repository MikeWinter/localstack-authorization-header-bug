#!/usr/bin/env bash
set -eu

function do_packaging() {
  local -r tmp_path="$1"
  local -r version="$2"

  # shellcheck disable=SC2154
  cd "$abs_script_path"
  poetry export --output "$tmp_path/requirements.txt"
  poetry run pip install --target "$tmp_path" --requirement "$tmp_path/requirements.txt"
  unzip -q "dist/auth_lambda-${version}-py3-none-any.whl" -d "$tmp_path"

  cd "$tmp_path"
  zip -q -r "$abs_script_path/dist/auth-lambda-${version}.zip" .
}
