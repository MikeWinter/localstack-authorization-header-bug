import json
from typing import Any, Mapping


def get_authorization_header(event: Mapping[str, Any], _context) -> Mapping[str, Any]:
    headers = _get_headers(event)

    if "authorization" not in headers:
        return {
            "statusCode": 401,
            "headers": {
                "WWW-Authenticate": 'Bearer realm="test"',
            },
            "body": json.dumps(
                {
                    "error": "No bearer token present",
                }
            ),
        }

    return {
        "statusCode": 200,
        "body": json.dumps(
            {
                "auth": headers["authorization"],
            }
        ),
    }


def _get_headers(event: Mapping[str, Any]) -> Mapping[str, str]:
    original_headers: Mapping[str, str] = event["headers"]
    headers = {}

    for key, value in original_headers.items():
        headers[key.lower()] = value

    return headers
